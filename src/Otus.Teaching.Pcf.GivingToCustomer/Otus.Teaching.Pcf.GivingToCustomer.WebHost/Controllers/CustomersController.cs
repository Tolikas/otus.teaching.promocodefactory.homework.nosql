﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;
using Otus.Teaching.Pcf.GivingToCustomer.WebHost.Mappers;
using Otus.Teaching.Pcf.GivingToCustomer.WebHost.Models;

namespace Otus.Teaching.Pcf.GivingToCustomer.WebHost.Controllers
{
    /// <summary>
    /// Клиенты
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class CustomersController : ControllerBase
    {
        private readonly IRepository<Customer> _customerRepository;
        private readonly IRepository<Preference> _preferenceRepository;
        private readonly IRepository<CustomerPreference> _customerPreferenceRepository;
        private readonly IRepository<PromoCode> _promocodeRepository;
        private readonly IRepository<PromoCodeCustomer> _promoCodeCustomerRepository;

        public CustomersController(
            IRepository<Customer> customerRepository, 
            IRepository<Preference> preferenceRepository,
            IRepository<CustomerPreference> customerPreferenceRepository,
            IRepository<PromoCode> promocodeRepository,
            IRepository<PromoCodeCustomer> promoCodeCustomerRepository)
        {
            _customerRepository = customerRepository;
            _preferenceRepository = preferenceRepository;
            _customerPreferenceRepository = customerPreferenceRepository;
            _promocodeRepository = promocodeRepository;
            _promoCodeCustomerRepository = promoCodeCustomerRepository;
        }
        
        /// <summary>
        /// Получить список клиентов
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<List<CustomerShortResponse>>> GetCustomersAsync()
        {
            var customers =  await _customerRepository.GetAllAsync();

            var response = customers.Select(x => new CustomerShortResponse
            {
                Id = x.Id,
                Email = x.Email,
                FirstName = x.FirstName,
                LastName = x.LastName
            }).ToList();

            return Ok(response);
        }
        
        /// <summary>
        /// Получить клиента по id
        /// </summary>
        /// <param name="id">Id клиента, например <example>a6c8c6b1-4349-45b0-ab31-244740aaf0f0</example></param>
        /// <returns></returns>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<CustomerResponse>> GetCustomerAsync(Guid id)
        {
            var customer =  await _customerRepository.GetByIdAsync(id);

            var preferences = (await _customerPreferenceRepository.GetWhere(p => p.CustomerId == id)).ToList();

            foreach (var item in preferences)
            {
                var preference  = await _preferenceRepository.GetByIdAsync(item.PreferenceId);

                item.Customer = customer;
                item.Preference = preference;
            }

            customer.Preferences = preferences;

            var promoCodes = (await _promoCodeCustomerRepository.GetWhere(p => p.CustomerId == id)).ToList();

            foreach (var item in promoCodes)
            {
                var promocode = await _promocodeRepository.GetByIdAsync(item.PromoCodeId);

                item.Customer = customer;
                item.PromoCode = promocode;
            }

            customer.PromoCodes = promoCodes;

            var response = new CustomerResponse(customer);

            return Ok(response);
        }
        
        /// <summary>
        /// Создать нового клиента
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult<CustomerResponse>> CreateCustomerAsync(CreateOrEditCustomerRequest request)
        {
            var preferences = await _preferenceRepository.GetRangeByIdsAsync(request.PreferenceIds);

            var customer = CustomerMapper.MapFromModel(request, preferences);

            await _customerRepository.AddAsync(customer);

            foreach (var preference in customer.Preferences)
            {
                await _customerPreferenceRepository.AddAsync(preference);
            }

            return CreatedAtAction(nameof(GetCustomerAsync), new {id = customer.Id}, customer.Id);
        }
        
        /// <summary>
        /// Обновить клиента
        /// </summary>
        /// <param name="id">Id клиента, например <example>a6c8c6b1-4349-45b0-ab31-244740aaf0f0</example></param>
        /// <param name="request">Данные запроса></param>
        [HttpPut("{id:guid}")]
        public async Task<IActionResult> EditCustomersAsync(Guid id, CreateOrEditCustomerRequest request)
        {
            var customer = await _customerRepository.GetByIdAsync(id);

            if (customer == null)
            {
                return NotFound();
            }

            var newPreferences = await _preferenceRepository.GetRangeByIdsAsync(request.PreferenceIds);
            
            CustomerMapper.MapFromModel(request, newPreferences, customer);

            var oldPreferences = (await _customerPreferenceRepository.GetWhere(p => p.CustomerId == id)).ToList();

            foreach (var item in oldPreferences)
            {
                await _customerPreferenceRepository.DeleteAsync(item);
            }

            foreach (var preference in customer.Preferences)
            {
                await _customerPreferenceRepository.AddAsync(preference);
            }

            await _customerRepository.UpdateAsync(customer);

            return NoContent();
        }

        /// <summary>
        /// Удалить клиента
        /// </summary>
        /// <param name="id">Id клиента, например <example>a6c8c6b1-4349-45b0-ab31-244740aaf0f0</example></param>
        [HttpDelete("{id:guid}")]
        public async Task<IActionResult> DeleteCustomerAsync(Guid id)
        {
            var customer = await _customerRepository.GetByIdAsync(id);

            if (customer == null)
            {
                return NotFound();
            }

            var preferences = (await _customerPreferenceRepository.GetWhere(p => p.CustomerId == id)).ToList();

            foreach (var item in preferences)
            {
                await _customerPreferenceRepository.DeleteAsync(item);
            }

            var promoCodes = (await _promoCodeCustomerRepository.GetWhere(p => p.CustomerId == id)).ToList();

            foreach (var item in promoCodes)
            {
                await _promoCodeCustomerRepository.DeleteAsync(item);
            }

            await _customerRepository.DeleteAsync(customer);

            return NoContent();
        }
    }
}