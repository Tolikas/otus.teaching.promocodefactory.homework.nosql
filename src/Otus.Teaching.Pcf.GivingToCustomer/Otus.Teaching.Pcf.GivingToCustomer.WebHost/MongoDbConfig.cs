﻿namespace Otus.Teaching.Pcf.GivingToCustomer.WebHost
{
    public class MongoDbConfig
    {
        public string Database { get; set; }
        public string Host { get; set; }
        public string Port { get; set; }
        public string User { get; set; }
        public string Password { get; set; }
        public string ConnectionString
        {
            get
            {
                var logPass = !string.IsNullOrWhiteSpace(User) && !string.IsNullOrWhiteSpace(Password)
                    ? $"{User}:{Password}@"
                    : "";

                return $"mongodb://{logPass}{Host}:{Port}";
            }
        }
    }
}