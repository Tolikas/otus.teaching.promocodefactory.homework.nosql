﻿using System;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace Otus.Teaching.Pcf.GivingToCustomer.Core.Domain
{
    public class CustomerPreference : BaseEntity
    {
        [BsonRepresentation(BsonType.String)]
        public Guid CustomerId { get; set; }

        [BsonIgnore]
        public virtual Customer Customer { get; set; }

        [BsonRepresentation(BsonType.String)]
        public Guid PreferenceId { get; set; }

        [BsonIgnore]
        public virtual Preference Preference { get; set; }
    }
}