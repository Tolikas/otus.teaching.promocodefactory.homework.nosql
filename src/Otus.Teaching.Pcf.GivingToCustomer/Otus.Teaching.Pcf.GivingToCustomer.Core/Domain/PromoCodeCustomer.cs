﻿using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Bson;
using System;

namespace Otus.Teaching.Pcf.GivingToCustomer.Core.Domain
{
    public class PromoCodeCustomer : BaseEntity
    {
        [BsonRepresentation(BsonType.String)]
        public Guid PromoCodeId { get; set; }

        [BsonIgnore]
        public virtual PromoCode PromoCode { get; set; }

        [BsonRepresentation(BsonType.String)]
        public Guid CustomerId { get; set; }

        [BsonIgnore]
        public virtual Customer Customer { get; set; }
    }
}