﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using MongoDB.Driver;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;

namespace Otus.Teaching.Pcf.GivingToCustomer.DataAccess.Repositories
{
    public class MongoRepository<T> : IRepository<T> where T : BaseEntity
    {
        private readonly IMongoDatabase _db;

        public MongoRepository(MongoDbContext context)
        {
            _db = context.Database;
        }
        
        protected IMongoCollection<T> Collection => _db.GetCollection<T>(typeof(T).Name);

        public async Task<IEnumerable<T>> GetAllAsync()
        {
            return (await Collection.FindAsync(p => true)).ToEnumerable();
        }

        public async Task<T> GetByIdAsync(Guid id)
        {
            return (await Collection.FindAsync(p => p.Id == id)).ToEnumerable().FirstOrDefault();
        }

        public async Task<IEnumerable<T>> GetRangeByIdsAsync(List<Guid> ids)
        {
            return (await Collection.FindAsync(p => ids.Contains(p.Id))).ToEnumerable();
        }

        public async Task<T> GetFirstWhere(Expression<Func<T, bool>> predicate)
        {
            var entity = Collection.AsQueryable().Where(predicate.Compile()).FirstOrDefault();
            return await Task.FromResult(entity);
        }

        public async Task<IEnumerable<T>> GetWhere(Expression<Func<T, bool>> predicate)
        {
            var entities = Collection.AsQueryable().Where(predicate.Compile());
            return await Task.FromResult(entities);
        }

        public async Task AddAsync(T entity)
        {
            await Collection.InsertOneAsync(entity);
        }

        public async Task UpdateAsync(T entity)
        {
            var filter = Builders<T>.Filter.Eq(p => p.Id, entity.Id);
            await Collection.ReplaceOneAsync(filter, entity);
        }

        public async Task DeleteAsync(T entity)
        {
            var filter = Builders<T>.Filter.Eq(p => p.Id, entity.Id);
            await Collection.DeleteOneAsync(filter);
        }
    }
}