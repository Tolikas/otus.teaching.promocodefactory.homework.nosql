﻿using Microsoft.EntityFrameworkCore;
using MongoDB.Bson.Serialization.Serializers;
using MongoDB.Bson.Serialization;
using MongoDB.Bson;
using MongoDB.Driver;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;
using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Teaching.Pcf.GivingToCustomer.DataAccess
{
    public class MongoDbContext
    {
        public MongoDbContext(IMongoClient client, string dbName)
        {
            Database = client.GetDatabase(dbName);
        }

        public IMongoDatabase Database { get; }
    }
}