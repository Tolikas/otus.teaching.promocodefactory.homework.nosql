﻿using System;
using MongoDB.Bson;
using MongoDB.Driver;
using System.Linq;

namespace Otus.Teaching.Pcf.GivingToCustomer.DataAccess.Data
{
    public class MongoDbInitializer : IDbInitializer
    {
        private readonly IMongoDatabase _db;

        public MongoDbInitializer(MongoDbContext context)
        {
            _db = context.Database;
        }

        public void InitializeDb()
        {
            #region Init Preference

            var collectionName = "Preference";

            if (!CollectionExists(collectionName))
            {
                var preferenceCollection = _db.GetCollection<BsonDocument>(collectionName);
                
                var preferenceDocuments = FakeDataFactory.Preferences
                    .Select(p => new BsonDocument
                    {
                        {"Id", p.Id.ToString()},
                        {"Name", p.Name}
                    });

                preferenceCollection.InsertMany(preferenceDocuments);
            }

            #endregion

            #region Init Customer

            collectionName = "Customer";

            if (!CollectionExists(collectionName))
            {
                var customerCollection = _db.GetCollection<BsonDocument>(collectionName);

                var customerDocuments = FakeDataFactory.Customers
                    .Select(p => new BsonDocument
                    {
                        {"Id", p.Id.ToString()},
                        {"Email", p.Email},
                        {"FirstName", p.FirstName},
                        {"LastName", p.LastName}
                    });

                customerCollection.InsertMany(customerDocuments);
            }

            #endregion

            #region Init CustomerPreference

            collectionName = "CustomerPreference";

            if (!CollectionExists(collectionName))
            {
                var customerPreferenceCollection = _db.GetCollection<BsonDocument>(collectionName);

                var customerPreferenceDocuments = FakeDataFactory.Customers
                    .SelectMany(p => p.Preferences)
                    .Select(p => new BsonDocument
                    {
                        {"Id", Guid.NewGuid().ToString()},
                        {"CustomerId", p.CustomerId.ToString()},
                        {"PreferenceId", p.PreferenceId.ToString()}
                    });

                customerPreferenceCollection.InsertMany(customerPreferenceDocuments);
            }

            #endregion

            #region Init PromoCode

            collectionName = "PromoCode";

            if (!CollectionExists(collectionName))
            {
                _db.CreateCollection(collectionName);
            }

            #endregion

            #region Init PromoCodeCustomer

            collectionName = "PromoCodeCustomer";

            if (!CollectionExists(collectionName))
            {
                _db.CreateCollection(collectionName);
            }

            #endregion
        }

        public bool CollectionExists(string collectionName)
        {
            var filter = new BsonDocument("name", collectionName);
            var collections = _db.ListCollections(new ListCollectionsOptions { Filter = filter });

            return collections.Any();
        }

    }
}